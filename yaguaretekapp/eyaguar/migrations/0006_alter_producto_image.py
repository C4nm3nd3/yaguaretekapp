# Generated by Django 3.2.4 on 2021-07-07 23:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('eyaguar', '0005_alter_producto_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producto',
            name='image',
            field=models.ImageField(upload_to='static/eyaguar/images/imagen_producto'),
        ),
    ]
