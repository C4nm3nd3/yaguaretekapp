from django.apps import AppConfig


class EyaguarConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'eyaguar'
