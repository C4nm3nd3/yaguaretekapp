from django.shortcuts import render

from django.http import HttpResponse

from .models import *        



def home(request):
    proconImagen = Producto.objects.all().order_by("-id")[:3]
    proSinImagen = Producto.objects.all().order_by("-id")[3:9]

    contexto={  "proconImagen": proconImagen,
                "proSinImagen": proSinImagen
                }
    return render(request, 'eyaguar/home.html',contexto)


def about(request):
    return render(request, 'eyaguar/about.html')

def createproduct(request):
    return render(request, 'eyaguar/createproduct.html')

def login(request):
    return render(request, 'eyaguar/login.html')

def productpage(request):
    return render(request, 'eyaguar/productpage.html')

def reguser(request):
    return render(request,'eyaguar/reguser.html')

def cart(request):
    return render(request,'eyaguar/cart.html')

def searchresults(request):
    return render(request,'eyaguar/searchresults.html')

def logout(request):
    return render(request, 'eyaguar/logout.html')








