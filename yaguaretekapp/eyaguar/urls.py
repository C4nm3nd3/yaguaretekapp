
from django.contrib import admin
from django.urls import path
from django.urls import path, include

from django.conf.urls.static import static
from django.conf import settings


from . import views

urlpatterns=[
    path('admin/',admin.site.urls),
    path('',views.home,name='home'),
    path('about',views.about, name='about'),
    path('createproduct',views.createproduct,name='createproduct'),
    path('login',views.login,name='login'),
    path('productpage',views.productpage,name='productpage'),
    path('reguser',views.reguser,name='reguser'),
    path('cart',views.cart,name='cart'),
    path('searchresults',views.searchresults,name='searchresults'),
    path('logout', views.logout,name='logout'),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)