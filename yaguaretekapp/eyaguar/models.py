from django.db import models
from django.contrib.auth.models import User
from django.db.models.base import Model

# Clase cliente usando el sistema user de django.
class Cliente(models.Model):
    user=models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    name=models.CharField(max_length=200, null=True)
    email=models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.name

#Clase pedido, para el carrito. Como puede tener multiples items, por eso uso Foreign Key

class Pedido(models.Model):
    cliente=models.ForeignKey(Cliente, on_delete=models.SET_NULL,blank=True, null=True)

class Categoria(models.Model):
    nombre=models.CharField(max_length=50)
    created=models.DateTimeField(auto_now_add=True)
    updated=models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.nombre



# Clase producto con los atributos

class Producto(models.Model):
    nombre = models.CharField(max_length=64)
    precio = models.FloatField(default=0)
    image = models.ImageField(null=True,blank=True)
    description = models.CharField(max_length=150)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, default=0)
    created=models.DateTimeField(auto_now_add=True)
    updated=models.DateTimeField(auto_now_add=True)
    stock=models.BooleanField(default=True)

    class Meta:
        verbose_name='producto'
        verbose_name_plural='productos'
    def __str__(self):
        return f"Producto: #{self.nombre} Descripcion: {self.description}, Precio: {self.precio}"


# clase item de pedido (orderitem) para cada componente en una orden (cantidad, producto, fecha de agregado <para ordenarlo>, que pedido es? )

class ItemPedido(models.Model):
    producto=models.ForeignKey(Producto, on_delete=models.SET_NULL,blank=True, null=True)
    pedido=models.ForeignKey(Pedido, on_delete=models.SET_NULL,blank=True, null=True)
    cantidad=models.IntegerField(default=0, null=True, blank=True)
    agregadoel=models.DateTimeField(auto_now_add=True)







